Download emacs with homebrew

$ brew install emacs

----------------------

If there are another previous version of emacs in your system

$ which emacs

/usr/bin/emacs

$ emacs --version

GNU Emacs 22.0.1
...
...

Then copy the following line to your ~/.bash.profile archive

alias emacs="/usr/local/bin/emacs"

For run the brew version of emacs.

-----------------------

Then make the configutation directory ~/.emacs.d and 
additionally ~/.emacs.d/lisp

$ mkdir ~/.emacs.d/lisp

copy from internet the archive linum+.el in this directory.
You can download the archive (along with too many others with git)

$ cd
$ git clone --branch master https://github.com/ezotrank/my_emacs

copy the archive linum+.el into the directory ~/.emacs.d/lisp

$ cd my_emacs/packages
$ cp -pr linum+.el ~/.emacs.d/lisp

Download the moe theme manually from the git site
issue the following command, at terminal, at ~/.emacs.d folder:

$ cd ~/.emacs.d
$ git clone --branch master https://github.com/kuanyui/moe-theme.el


Then copy the init.el archive of this folder in your system
at folder ~/.emacs.d

Or edit a new ~/.emacs.d/init.el initialization archive 
with the following lines:

;; setting line numbers
(add-to-list 'load-path "~/.emacs.d/lisp")
(require 'linum+)  ;; loading the linum extention
(global-linum-mode t) ;; setting linum-mode to true
(setq linum-format "%4d \u2502 ") ;; putting the format

;; setting the moe theme
(add-to-list 'load-path "~/.emacs.d/moe-theme.el")
(require 'moe-theme)
(moe-light)

And then you can use emacs with line numbers and with the 
beautiful moe light theme



