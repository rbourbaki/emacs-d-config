;; setting line numbers
(add-to-list 'load-path "~/.emacs.d/lisp")
(require 'linum+)  ;; loading the linum extention
(global-linum-mode t) ;; setting linum-mode to true
(setq linum-format "%4d \u2502 ") ;; putting the format

;; setting the moe theme
(add-to-list 'load-path "~/.emacs.d/moe-theme.el")
(require 'moe-theme)
(moe-light)